/*
 *  @desc 它就是将来真正要用到的数据，我们将其统一放置在reducers.js文件
 */

import { defaultState } from './state';
import * as actions from './action'
const initState: defaultState = {
    user: window.sessionStorage.getItem('user'),
    token: window.sessionStorage.getItem('token'),
    chatList: []
}
export function Auth(state = initState, action: actions.defaultAction){
    switch(action.type){
        case 'SET_TOKEN':
            return { ...state, token: action.payload }
        case 'SET_USER':
            return { ...state, user: action.payload }
        case 'SET_LOGOUT':
            return { user: null, token: null }
        default:
            return { ...state }
    }
}
export function getList(state = initState, action: actions.defaultAction) {
    switch (action.type) {
        // ...
    }
}

