/*
 *  @tmpl 个人中心模板
 */

import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Store } from '@ngrx/store'
import * as actions from '../../ngrx/action'
declare var wcPop: any
@Component({
    selector: 'app-ucenter',
    template: `
        <div class="wc__ucenter-head"> <div class="bg"></div> <div class="hdbar">
                <i class="iconfont icon-dianzan"></i> <i class="iconfont icon-dots"></i></div>
        </div>
        <div class="wc__ucenter-list"> <ul class="clearfix"> <li> <div class="item flexbox flex-alignc wcim__material-cell"> <img class="uimg" src="../../assets/img/uimg/u__chat-img06.jpg" />
                        <span class="txt flex1"><em class="fs-36">Angle</em><em class="iconfont icon-female fs-32 ml-10" style="color:#f37e7d"></em><i>+86 {{auth.user}}</i></span>
                        <i class="iconfont icon-arrR c-ccc fs-32"></i> </div>
                </li> <li> <div class="item flexbox flex-alignc wcim__material-cell"><span class="txt flex1">动态</span><em class="wcim__badge wcim__badge-dot"></em><i class="iconfont icon-arrR c-ccc fs-32"></i>
                    </div>
                </li> <li>
                    <div class="item flexbox flex-alignc wcim__material-cell"> <span class="txt flex1">二维码名片</span><i class="iconfont icon-arrR c-ccc fs-32"></i>
                    </div> <div class="item flexbox flex-alignc wcim__material-cell"> <span class="txt flex1">我的收藏</span><i class="iconfont icon-arrR c-ccc fs-32"></i>
                    </div> <div class="item flexbox flex-alignc wcim__material-cell" (click)="aboutSys()"> <span class="txt flex1">关于</span><em class="lbl mr-5">版本1.0.0</em><i class="iconfont icon-arrR c-ccc fs-32"></i>
                    </div> </li>
                <li> <div class="item flexbox flex-alignc wcim__material-cell" (click)="logoutSys()"> <span class="txt flex1 align-c" style="color:#ff3b30">退出登录</span>
                    </div>  </li>
            </ul>
        </div >
    `,
    styles: [``]
})
export class UcenterComponent implements OnInit {
    ngOnInit(): void {
        // throw new Error("Method not implemented.");
    }
    private auth: any
    constructor(
        private router: Router, private store: Store<{}>
    ) {
        let that = this
        this.store.select('auth').subscribe(v => { that.auth = v;
        })
    }
    aboutSys(){
        let _aboutIdx = wcPop({
            skin: 'ios', content: '<img src="../../assets/img/uimg/u__qun-angular.png" style="margin-bottom:10px;height:80px;" /><div class="c-45cff5 mb-10" style="font-size:20px;">Angular版仿微信聊天室</div><div class="c-5a5b5c mb-20" style="font-size:14px;font-family:楷体;">基于angular+angular-cli+angular-router+ngrx技术架构</div>',
            shadeClose: true, xclose: true, btns: [
                { text: '知道了', style: 'color:#378fe7;', onTap() { wcPop.close(_aboutIdx);
                    }
                }
            ]
        });
    }
    logoutSys(){
        let that = this
        let _logoutIdx = wcPop({
            skin: 'android', title: '提示', content: '确定要退出聊天室嚒？', btns: [ { text: '取消', onTap() {
                        wcPop.close();
                    }
                }, { text: '退出', style: 'color:#ff3b30', onTap() { that.store.dispatch(new actions.logout())
                        that.router.navigate(['/login'])
                        wcPop.close();
                    }
                }
            ]
        });
    }
}
