
import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { routes } from '../../app/app-routing.module';
@Component({
    selector: 'tab-bar',
    template: `
        <div class="wcim__tabBar" *ngIf="isShow">
            <div class="bottomfixed wcim__borT">
                <ul class="flexbox flex-alignc">
                    <li class="flex1"><a routerLink="/index" routerLinkActive="on" routeLinkActiveOptions="{exact: true}"><span class="ico"><i class="iconfont icon-tabbar_xiaoxi2"></i><em class="wcim__badge">18</em></span><span class="txt">消息</span></a></li>
                    <li class="flex1"><a routerLink="/contact" routerLinkActive="on"><span class="ico"><i class="iconfont icon-tabbar_tongxunlu"></i></span><span class="txt">通讯录</span></a></li>
                    <li class="flex1"><a routerLink="/ucenter" routerLinkActive="on"><span class="ico"><i class="iconfont icon-tabbar_wo2"></i><em class="wcim__badge wcim__badge-dot"></em></span><span class="txt">我的</span></a></li>
                </ul>
            </div>
        </div>
    `,
    styles: [`
        .wcim__tabBar{padding-top: 1.1rem; position: relative;}.wcim__tabBar .bottomfixed{background: #fbfeff; max-width:750px; width: 100%; position: fixed; bottom: 0; z-index: 1001;}
        .wcim__tabBar ul li{text-align:center; height: 1.1rem;}.wcim__tabBar ul li a{display: block;}
        .wcim__tabBar ul li .ico{display: flex; align-items: center; justify-content: center; margin: 0 auto; margin-top: .1rem; height: .5rem; width: .5rem; position: relative;}
        .wcim__tabBar ul li .iconfont{color:#9e9e9e; font-size: .45rem;}.wcim__tabBar ul li a.on .iconfont{color:#1976d2;}.wcim__tabBar ul li .txt{color: #9e9e9e; display: block; font-size: .24rem;}
        .wcim__tabBar ul li a.on .txt{color: #1976d2;}.wcim__tabBar ul li .ico .wcim__badge{position: absolute; top: 0; left: .4rem;}
        .wcim__tabBar ul li .ico .wcim__badge-dot{left: .4rem;}
    `]
})
export class TabBarComponent {
    private isShow: boolean
    constructor(private router: Router,
    ) {
        router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                let curPath = event.url.split('/').join('').replace('#', '');
                routes.map((item, index) => {
                    let routePath = item.path.split('/').join('').replace('#', '');
                    if(curPath === routePath){this.isShow = item.data && item.data.showTabBar ? true : false
                    }
                })
            }
        });
    }
}
